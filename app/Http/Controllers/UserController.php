<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::paginate();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	try{
        if ( User::where('email', $request->email)->first() ) {
		 return ['success'=>0,'error'=>'email_exists'];
        }
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->api_token = Str::random(60);
        $save_user = $user->save();
        if($save_user){
            return ['success'=>1,'user'=>$user];
        }
            else{
                    return ['success'=>0];
            }
        }catch (Exception $e) {
                return ['success'=>0,'error'=>$e->getCode()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::where('id', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edited = false;
        $user = User::where('id', $id)->get()[0];
        if( isset( $request->name ) && $request->name != $user->name )
        { 
            $user->name = $request->name;
            $edited = true;
        }
        if( isset( $request->email ) && $request->email != $user->email )
        { 
            $user->email = $request->email;
            $edited = true;
        }
        if( isset( $request->password )  )
        { 
            $user->password = Hash::make($request->password);
            $edited = true;
        }
        if( $edited ){
            $user->save();
            return [ 'success' => 1,'user'=>$user] ;
        }
        return [ 'success' => 0];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return [ 'success'=> User::where('id', $id)->delete() ];
    }
}
