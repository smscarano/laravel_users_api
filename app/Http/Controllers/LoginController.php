<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    public function token(Request $request)
    {
    $email = $request->email;
	$user = User::where('email' ,  $email )->first();
	
	if( $user ){
		$hash = Hash::check($request->password, $user->password);
		if( $hash ){
			$user->api_token = Str::random(60);
			$user->save();
			return [ 'success' => 1 , 'user' => $user ];
		}
		
	}
	return ['success'=>0];

    }
}
